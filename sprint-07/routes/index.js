import entityController from "./entity.js";
import fileUploaderController from "./fileUploader.js";
import peopleController from "./people.js";
import authController from "./auth.js";
import { verifyToken } from "../helpers.js";
import * as accessControl from "../roles.js";

export const registerExpressRoutes = (app) => {
  app.use(async (req, res, next) => {
    const authToken = req.headers["authorization"];
    if (!authToken) {
      return next();
    }
    try {
      const user = await verifyToken(authToken);
      req.loggedInUser = user;
      next();
    } catch (err) {
      res.status(500).send("Internal Server Error");
      next();
      return;
    }
  });

  app.get("/", (req, res) => {
    res.json({
      message: "Hey there!",
    });
  });

  app.get("/protected", accessControl.allowIfLoggedIn, (req, res) => {
    res.json({
      message: "You are logged in!",
      user: req.loggedInUser,
    });
  });

  app.get(
    "/protected-testing",
    accessControl.grantAccess("updateAny", "profile"),
    (req, res) => {
      res.json({
        message: "You can updateAny any profile!",
        user: req.loggedInUser,
      });
    }
  );

  entityController(app);
  fileUploaderController(app);
  peopleController(app);
  authController(app);
};
