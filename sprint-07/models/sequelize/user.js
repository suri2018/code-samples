import { Model, DataTypes } from "sequelize";

export default (sequelize) => {
  return sequelize.define("user", {
    name: DataTypes.TEXT,
    favoriteColor: {
      type: DataTypes.TEXT,
      defaultValue: "purple",
    },
    age: DataTypes.INTEGER,
    cash: DataTypes.INTEGER,
  });
};
