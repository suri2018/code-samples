import React from "react";
import PropTypes from "prop-types";

export const Counter = ({ count, decrement, increment }) => (
  <>
    <p>
      The sum of {count} + 3 = {count + 3}
    </p>
    <p>My current count is: {count}</p>
    <button onClick={decrement}>-</button>
    <button onClick={increment}>+</button>
  </>
);

Counter.displayName = "Counter";

Counter.propTypes = {
  count: PropTypes.number.isRequired,
  decrement: PropTypes.func.isRequired,
  increment: PropTypes.func.isRequired,
};

Counter.defaultProps = {
  count: 0,
  increment: () => {
    console.log("Not yet implemented!");
  },
  decrement: () => {
    console.log("Not yet implemented!");
  },
};
