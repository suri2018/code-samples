import React from "react";
import { observer, useLocalStore } from "mobx-react";
import "./App.css";
import useStore from "./store";

function App() {
  const store = useStore();

  const appState = useLocalStore(() => {
    return {
      label: "",
      setLabel(val) {
        appState.label = val;
      },
    };
  });

  const handleAddTodo = () => {
    store.addTodo(appState.label);
    appState.setLabel("");
  };

  const handleFilter = (ev) => {
    store.changeFilter(ev.target.value);
  };

  return (
    <div className="App">
      <input
        onChange={(ev) => appState.setLabel(ev.target.value)}
        value={appState.label}
      />
      <ul>
        {store.currentTodos.map((todo) => (
          <li
            key={todo.id}
            onClick={() => todo.toggle()}
            style={{
              textDecoration: todo.completed ? "line-through" : undefined,
            }}
          >
            {todo.label}
          </li>
        ))}
      </ul>

      <ul>
        {store.people.map((person) => (
          <li key={person.id} onClick={() => person.sayHi()}>
            {person.name}
          </li>
        ))}
      </ul>

      <button onClick={handleAddTodo}>Add Todo</button>
      <select onChange={handleFilter} value={store.currentFilter}>
        <option value="all">All</option>
        <option value="completed">Completed</option>
        <option value="not-completed">Not Completed</option>
      </select>
      <button onClick={() => store.makeNetworkRequest()}>Fetch people!</button>
    </div>
  );
}

export default observer(App);
