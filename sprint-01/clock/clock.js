class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date(),
    };
    setInterval(this.updateTime, 5000);
  }

  updateTime = () => {
    this.setState((state) => ({ time: new Date() }));
  };

  render() {
    return (
      <React.Fragment>
        <Time date={this.state.time} format="fancy" />
        <hr />
        <Time date={this.state.time} />
      </React.Fragment>
    );
  }
}

// This happens in the background in React land
/*
if (someFunction.extendsReactComponent) {
  someFunction.render();
} else {
  someFunction();
}
*/
