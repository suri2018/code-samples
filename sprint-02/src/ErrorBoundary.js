import React from "react";

class ErrorBoundary extends React.Component {
  state = {
    hasError: false,
  };

  static getDerivedStateFromError(err) {
    console.log(`Error from getDerivedStateFromError`);
    return {
      hasError: true,
    };
  }

  componentDidCatch(error, info) {
    console.log("Man, I'm broken!");
    console.log(error);
    console.log("Info", info);
  }

  render() {
    if (this.state.hasError) {
      return "Something Went Wrong";
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
